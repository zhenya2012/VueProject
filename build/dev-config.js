const webpackmerge = require("webpack-merge");
const baseconfig = require("./base.config");
module.exports =webpackmerge(baseconfig, {
    devServer: {
        contentBase: "./dist",
        inline: true,

    }
});