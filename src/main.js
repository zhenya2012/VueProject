//CommonJs 导入
const math = require("./js/MathUtils");
console.log(math.add(10,20));
console.log(math.mul(10,20));


//ES6导入
import { age,name,height} from "./js/info";

console.log(age);
console.log(name);
console.log(height);


//引入css文件
require("./css/normal.css");

//引入less文件
require("./css/special.less");
document.writeln('<div>hello world!</div>');


import Vue from 'vue'

const App={
    template:"<h2>{{name}}</h2>",
    data(){
        return{name:"哈哈哈哈 我是组件"}
    }
};

new Vue({
    el:"#app",
    template:'<div id="app">{{name}}<App/></div>',
    data:{name:'yaya'},
    components:{App}

});

